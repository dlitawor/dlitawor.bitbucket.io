'use strict';

$(function() {
    var data = {
        week: {
            countScore: '38',
            achivementRequirement: 12,
            currentStreak: 4,
            bestStreak: 12,
        },
        month: {
            countScore: '12',
            achivementRequirement: 186,
            currentStreak: 7,
            bestStreak: 44,
        },
        users: [
            {
                name: 'Walter Wynne',
                weeklyRank: {
                    current: 1,
                    previous: 2
                },
                monthlyRank: {
                    current: 3,
                    previous: 5
                },
                weeklyDistance: 105,
                monthlyDistance: 250,
            },
            {
                name: 'Annabel Ferdinand',
                weeklyRank: {
                    current: 2,
                    previous: 1
                },
                monthlyRank: {
                    current: 4,
                    previous: 3
                },
                weeklyDistance: 52,
                monthlyDistance: 234,
            },
            {
                name: 'Marty McFly',
                weeklyRank: {
                    current: 3,
                    previous: 3
                },
                monthlyRank: {
                    current: 2,
                    previous: 1
                },
                weeklyDistance: 50,
                monthlyDistance: 278,
            },
            {
                name: 'You!',
                weeklyRank: {
                    current: 7,
                    previous: 8
                },
                monthlyRank: {
                    current: 1,
                    previous: 4
                },
                weeklyDistance: 38,
                monthlyDistance: 323,
            },
        ]
    }

    function setData(range, data) {
        var dataContainersIds = Object.keys(data);
        dataContainersIds.map(function(item) {
            return $('#' + item).text(data[item])
        });

        var COUNTER_MAX = '490';
        var COUNTER_CALCULATED_MAX = '50';
        var counterModifier = Math.round((COUNTER_MAX / COUNTER_CALCULATED_MAX) * data.countScore);

        $('#goalCounter svg path:nth-of-type(2)').attr({'stroke-dasharray': counterModifier.toString().concat(', ' + COUNTER_MAX + '')})
        $('#leaderboardTable').html(renderLeaderboard(range));
    }

    function renderLeaderboard(rankingRange) {
        var users = data.users;
        var rankingRangeKey = rankingRange === 'week' ? 'weeklyRank' : 'monthlyRank';
        var distanceRangeKey = rankingRange === 'week' ? 'weeklyDistance' : 'monthlyDistance';

        function sortUsers(arr, key) {
            return arr.sort(function(a, b) {
                var x = a[key].current; var y = b[key].current;
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            })
        }

        sortUsers(users, rankingRangeKey);

        return users.map(function(item) {
            var currentRanking = item[rankingRangeKey].current;
            var previousRanking = item[rankingRangeKey].previous;
            var rankingStatus = '';

            if (currentRanking < previousRanking) {
                rankingStatus = 'rank-up'
            } else if (currentRanking === previousRanking) {
                rankingStatus = 'rank-same'
            } else {
                rankingStatus = 'rank-down'
            }

            var myScore = item.name === 'You!' ? 'my-score' : '';

            return (
                '<li class="leaderboard-row ' + rankingStatus + ' ' + myScore + '">' +
                    '<div class="leaderboard-rank">' + currentRanking + '</div>' +
                    '<div class="leaderboard-person">' + item.name + '</div>' +
                    '<div class="leaderboard-score-wrap">' +
                        '<div class="leaderboard-score">' + item[distanceRangeKey] + 'm</div>' +
                        '<div class="leaderboard-rank-change ' + rankingStatus + '"></div>' +
                    '</div>' +
                '</li>'
            )
        })
    }

    setData('week', data['week']);

    $('#timeRange').change(function(e) {
        setData(e.target.value, data[e.target.value]);
    })
})